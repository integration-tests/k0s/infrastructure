
# required_version = ">= 0.12"

# Size in bytes (20 GB)
# size = 21474836480
# Size in bytes (10 GB)
# size = 10737418240
# Size in bytes (8 GB)
# size = 8589934592

variable "servers" {

  type = map(map(string))

  default = {
    controller-1 = {
      memory    = 2048
      vcpu      = 1
      disk_size = "4294967296"
      octetIP   = 11
      hostname  = "controller-1"
    }
    controller-2 = {
      memory    = 2048
      vcpu      = 1
      disk_size = "4294967296"
      octetIP   = 12
      hostname  = "controller-2"
    }
    controller-3 = {
      memory    = 2048
      vcpu      = 1
      disk_size = "4294967296"
      octetIP   = 13
      hostname  = "controller-3"
    }

    worker-1 = {
      memory    = 2048
      vcpu      = 1
      disk_size = "8589934592"
      octetIP   = 21
      hostname  = "worker-1"
    }
    worker-2 = {
      memory    = 2048
      vcpu      = 1
      disk_size = "8589934592"
      octetIP   = 22
      hostname  = "worker-2"
    }
    worker-3 = {
      memory    = 2048
      vcpu      = 1
      disk_size = "8589934592"
      octetIP   = 23
      hostname  = "worker-3"
    }

  }
}
