# KVM Provider

## install provider

```bash
terraform init

.terraform/plugins/linux_amd64/terraform-provider-libvirt --version
```

## configure KVM / QEMU

check if you are in the `libvirt` group:
```bash
sudo getent group | grep libvirt
```
otherwise add:
```bash
sudo usermod -a -G libvirt $(whoami)
```

change permissions for qemu and restart service:
```bash
cat << EOF >> /etc/libvirt/qemu.conf
user = "libvirt-qemu"
group = "libvirt"
EOF

service libvirtd restart
```

## Cloud Images

You can find varios cloud images for distributions like *ubuntu* or *centos*

* **tested** images

     * [CentOS](https://cloud.centos.org/centos)
     * [Ubuntu](https://cloud-images.ubuntu.com)
     * [Debian](https://cdimage.debian.org/cdimage/openstack)

* **untested** images

     * [Devuan](https://files.devuan.org/devuan_ascii/virtual/)
     * Arch Linux

### defined pool

```bash
sudo mkdir /var/lib/libvirt/pool
virsh pool-define-as pool  --type dir --target /var/lib/libvirt/pool
virsh pool-autostart pool
virsh pool-start pool
```

restart the `libvirtd` service
(`virsh pool-start pool` or `systemctl restart libvirtd`)

### Download


#### centos

Take a look in the [repositoy](https://cloud.centos.org/centos/7/images/)

**1907**
```bash
sudo curl -sL https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1907.qcow2 --output /var/lib/libvirt/pool/CentOS-7-x86_64-GenericCloud-1907.qcow2
```

**latest 7 generic**
```bash
sudo curl -sL https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2 --output /var/lib/libvirt/pool/CentOS-7-x86_64-GenericCloud-latest.qcow2
```


#### ubuntu

Take a look in the [repositoy](https://cloud-images.ubuntu.com/)

**1904 / disco**
```bash
sudo curl -s https://cloud-images.ubuntu.com/disco/current/disco-server-cloudimg-amd64.img --output /var/lib/libvirt/pool/ubuntu-1904-server-cloudimg-amd64.qcow2
```
**1910 / eoan**
```bash
sudo curl -sL https://cloud-images.ubuntu.com/eoan/current/eoan-server-cloudimg-amd64.img   --output /var/lib/libvirt/pool/ubuntu-1910-server-cloudimg-amd64.qcow2
```


#### debian

**9 / stretch**
```bash
sudo curl -s https://cdimage.debian.org/cdimage/openstack/current-9/debian-9-openstack-amd64.qcow2 --output /var/lib/libvirt/pool/debian-9-amd64.qcow2
```

**10 / buster**
```bash
sudo curl -s https://cdimage.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2 --output /var/lib/libvirt/pool/debian-10-amd64.qcow2
```

#### to test

```bash
# https://cdimage.debian.org/cdimage/openstack/current-10/
# https://files.devuan.org/devuan_ascii/virtual/devuan_ascii_2.0.0_amd64_qemu.qcow2.xz
```


## usage

The default IP Range are `192.168.124.0/24`
The default Domain are `virt.local`

create an config-file.
**(If the Makefile is used, it must be exactly the same as the directory from which the Makefile is called!)**

```bash
cat terraform-libvirt.tfvars
servers = {
  "srv-01" = {
    "memory"    = 16384             # 16 GiB RAM
    "vcpu"      = 2                 #  2 CPU Cores
    "disk_size" = "64424509440"     # 60 GiB HDD
    octetIP     = 10                # 192.168.124.10
    "hostname"  = "build"           # Hostname
  }
  "srv-02" = {
    "memory"    = 2048              # 2 GiB RAM
    "vcpu"      = 2                 # s CPU Cores
    "disk_size" = "21474836480"     # 20 GiB HDD
    octetIP     = 11                # 192.168.124.11
    "hostname"  = "harbor"          # Hostname
  }
}
```

### Example

```bash
make init

terraform init

Initializing the backend...

Initializing provider plugins...
- Finding latest version of dmacvicar/libvirt...
- Finding latest version of hashicorp/template...
- Installing dmacvicar/libvirt v0.6.3...
- Installed dmacvicar/libvirt v0.6.3 (unauthenticated)
- Installing hashicorp/template v2.2.0...
- Installed hashicorp/template v2.2.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

```bash
make plan


make apply


make show


make destroy
```

## TODOs

- better support for other distributions
- better way for `cloud_init.cfg`

