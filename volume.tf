
/** other distributions **/

// debian 10
resource "libvirt_volume" "debian_10_image" {
  name     = "debian-image-10"
  pool     = "pool"
  source   = "/var/lib/libvirt/pool/debian-10-amd64.qcow2"
}

// debian 11
resource "libvirt_volume" "debian_11_image" {
  name     = "debian-image-11"
  pool     = "pool"
  source   = "/var/lib/libvirt/pool/debian-11-amd64.qcow2"
}

resource "libvirt_volume" "diskimage" {

  for_each       = var.servers

  pool           = "default"
  name           = "${each.key}.qcow2"

  base_volume_id = libvirt_volume.debian_11_image.id

  size           = lookup(var.servers[each.key], "disk_size", "") == "" ? "0" : var.servers[each.key].disk_size
}
