
resource "libvirt_network" "vm_network" {
  name      = "k0s_network"
  domain    = "k0s.local"
  addresses = ["192.168.125.0/24"]
  dns {
    enabled    = true
    local_only = true
  }
}
